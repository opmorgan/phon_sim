from lib.config import *
import csv
import pandas as pd
import numpy as np
from numpy import dot
from numpy.linalg import norm

def load_csv(fname):
    print(f'Loading file: \n   {datadir}/{fname}')

    df = pd.read_excel(f'{datadir}/{fname}')
    df.iloc[:, [1, 3, 5, 7, 9, 11]] = np.nan

    return df

def load_space(space_path):
    space = dict()
    for line in open(space_path, encoding='latin1'):
        line = line.strip()
        word, vec_raw = line.split("  ")
        word = word.lower()
        space[word] = np.array([float(x) for x in vec_raw.split()])
    return space

# cosine similarity
def cosine(v1, v2):
    if norm(v1) > 0 and norm(v2) > 0:
        return dot(v1, v2) / (norm(v1) * norm(v2))
    else:
        return 0.0

