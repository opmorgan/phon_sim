# specify input
datadir = ('/Volumes/CNRLab/COWA/Phonological Clustering')

#subjects = [
#        'a_002',
#        'a_010',
#        'a_014',
#        'a_032',
#        'a_043'
#        ]

subjects = [
        'a_002',
        'a_010',
        'a_014',
        'a_032',
        'a_043',
        'a_046',
        'a_047',
        'a_051',
        'a_053',
        'a_054',
        'a_057',
        'a_058',
        'a_061',
        'a_063',
        'a_064',
        'a_069',
        'a_071',
        'a_073',
        'a_075',
        'a_076',
        'a_078',
        'a_079',
        'a_080',
        'a_081',
        'a_085',
        'a_086',
        'a_c_013',
        'a_c_016',
        'a_c_017',
        'a_c_022',
        'a_c_024',
        'a_c_028',
        'a_c_030',
        'a_c_032',
        'a_c_033',
        'a_c_034',
        'a_c_036',
        'a_c_038',
        'a_c_041',
        'LyC_01',
        'LyC_02',
        'LyC_03',
        'LyC_04',
        'LyC_05',
        'LyC_06',
        'LyC_07',
        'LyC_08',
        'LyC_09',
        'LyC_10',
        'LyC_11',
        'LyC_12',
        'LyC_13',
        'LyC_14',
        'LyC_15',
        'LyC_16',
        'naf_a_01',
        'naf_a_02',
        'naf_a_03',
        'naf_a_04',
        'naf_a_06',
        'naf_a_07',
        'naf_a_08',
        'naf_a_09',
        'naf_a_10',
        'naf_a_11',
        'naf_c_02',
        'naf_c_04',
        'naf_c_06',
        'NIDA_09',
        'NIDA_10',
        'NIDA_13',
        'NIDA_17',
        'NIDA_19',
        'NIDA_36',
        'NIDA_43',
        'NIDA_45',
        'NIDA_46'
        ]



fnames = []
for j in range(len(subjects)):
    fnames.append(f'COWA Clusters - {subjects[j]}.xlsx')

# specify path to phonetic space vectors
space_path = 'data/cmudict-0.7b-with_phrases_etc_simvecs_3'

# specify output
output_dir = 'results'
