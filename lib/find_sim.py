from lib.config import *
from lib.utils import load_csv, load_space, cosine
import numpy as np

def find_sim():
    # load pre-compiled phonetic space vectors
    print(f'Loading pre-compiled phonetic space vectors ({space_path})')
    space = load_space(space_path)
    print('Space vectors loaded.')

    for f in range(len(fnames)):
        df = load_csv(fnames[f])
        print('calculating similarities...')


        x = [0, 2, 4, 6, 8, 10]
        for j in x:
            for row in range(len(df)):
                if row > 0 and isinstance(df.iloc[row, j], str):
                    words = [f'{df.iloc[row-1,j]}', f'{df.iloc[row,j]}']

                    # check if input word exists in vector space
                    if words[0] in space.keys() and words[1] in space.keys():
                        dist = cosine(space[words[0]], space[words[1]])
                    # if either of the words isn't in your dictionary, return nan
                    else: 
                        dist = np.nan
                        for k in [0, 1]:
                            if not words[k] in space.keys():
                                print(f'word not found: {words[k]}')

                    df.iloc[row,j+1] = dist

        output_name = (f'phone_sim_{subjects[f]}.csv')
        output_path = (f'{output_dir}/{output_name}')

        print(f'Saving file: \n   {output_path}.')
        df.to_csv(output_path, encoding='utf-8', index=False)
