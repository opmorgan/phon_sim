Find cosine similarity between pairs of words using Allison Parrish's phonetic vector space (Parrish, 2017). For the Marvel Lab's COWA data, Winter 2018-19.

Takes as input xlxs files with 6 columns of strings, alternating with 6 columns of NaNs (Starting with strings). Specify input path(s) in config.py.
